<h2 align="center">
<br>
Flights Fullstack Booking Platform
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/angularjs/angularjs-original.svg" alt="react" width="40" height="40"/>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon@latest/icons/dotnetcore/dotnetcore-original.svg" alt="dotnetcore" width="40" height="40"/>
</h2>

<h3 align="center">Angular/Typescript/Entity Framework/.NET Core</h3>

<div align="left">
   <a href="#">
   <img align="left" src="./readme-img.png" width="100%" height="320" style="margin-bottom: 10px;">
   </a>
</div>
<br />

## Frontend Models:

- **Open API for Generating Typescript Models**: Backend generates swagger json documentationusing Swashbuckle, which allows the frontend to generate TypeScript models and services using the ng-openapi-gen library. Ensures
  the frontend models and services are always in sync with the backend API definitions, reducing the likelihood of errors and improving the development workflow.

<div align="left">
   <a href="#">
   <img align="left" src="./readme-img2.png" width="100%" height="140" style="margin-bottom: 10px;">
   </a>
</div>
<br />

## Backend Architecture:

- **Dtos**: DTOs (Data Transfer Objects) are used to encapsulate data and transfer it between different layers of the application.
- **Read Models**: Represents the data structures used to retrieve information from the system.
- **Controllers**: The FlightController is responsible for managing flight-related operations.
