import { Component } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FlightService } from "../api/services/flight.service";
import { FlightRm} from "../api/models/flight-rm";

@Component({
  selector: 'app-book-flight',
  templateUrl: './book-flight.component.html',
  styleUrls: ['./book-flight.component.css']
})
export class BookFlightComponent {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private flightService: FlightService) {}

  flightId: string = "not loaded"
  flight: FlightRm = {}

  ngOnInit(): void {
    // get flight id based on route
    this.route.paramMap
      .subscribe(p => this.findFlight(p.get("flightId")))
      // .subscribe(p => this.flightId = p.get("flightId") ?? "not passed")
  }

  // set flight id provided by param
  private findFlight = (flightId: string | null) => {
    this.flightId = flightId ?? 'not passed';

    // set flight based on api result
    this.flightService.findFlight({ id: this.flightId })
      .subscribe(flight => this.flight = flight,
        this.handleError)
  }

  private handleError = (err: any) => {
    // redirect if flight not found
    if (err.status == 404) {
      alert("Flight not found!")
      this.router.navigate(['/search-flights'])
    }


    if (err.status == 409) {
      console.log("err: " + err);
      alert(JSON.parse(err.error).message)
    }

    console.log("Response Error. Status: ", err.status)
    console.log("Response Error. Status Text: ", err.statusText)
    console.log(err)
  }
}
