import { Component, OnInit } from '@angular/core';
import { FlightService} from "../api/services/flight.service"; // generated from open api
import { FlightRm} from "../api/models/flight-rm";
import { FormBuilder } from '@angular/forms';
import { Time } from '@angular/common';
@Component({
  selector: 'app-search-flights',
  templateUrl: './search-flights.component.html',
  styleUrls: ['./search-flights.component.css']
})
export class SearchFlightsComponent implements OnInit{
  constructor(private flightService: FlightService) {}// inject FlightService - http requests
  ngOnInit() {}

  searchResult: FlightRm[] = []
  search() {
    this.flightService.searchFlight({})
      .subscribe(response => this.searchResult = response,
        this.handleError)
  }

  private handleError(err: any) {
    console.log("Response Error. Status: ", err.status)
    console.log("Response Error. Status Text: ", err.statusText)
    console.log(err)
  }
  // searchResult: FlightRm[] = [
  //   {
  //     id: "0",
  //     airline: "American Airlines",
  //     remainingNumberOfSeats: 500,
  //     departure: { time: Date.now().toString(), place: "Los Angeles" },
  //     arrival: { time: Date.now().toString(), place: "Istanbul" },
  //     price: "350"
  //   },
  //   {
  //     id: "1",
  //     airline: "Deutsche BA",
  //     remainingNumberOfSeats: 60,
  //     departure: { time: Date.now().toString(), place: "Munchen" },
  //     arrival: { time: Date.now().toString(), place: "Schiphol" },
  //     price: "600"
  //   },
  //   {
  //     id: "2",
  //     airline: "British Airways",
  //     remainingNumberOfSeats: 100,
  //     departure: { time: Date.now().toString(), place: "London, England" },
  //     arrival: { time: Date.now().toString(), place: "Vizzola-Ticino" },
  //     price: "500"
  //   }
  // ]
}

// flight read model provides flight details
// open api is also used to generate the interfaces automatically

// export interface FlightRm {
//   airline: string;
//   arrival: TimePlaceRm;
//   departure: TimePlaceRm;
//   price: string;
//   remainingNumberOfSeats: number;
// }

// place and time info combined

// export interface TimePlaceRm {
//   place: string;
//   time: string;
// }
